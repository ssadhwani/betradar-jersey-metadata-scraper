import asyncio
import datetime
import json
import time
import timeit

import requests_async as requests
import xmltodict

headers = {
    'x-access-token': 'LeqIGIjExP3xFUQdD8'
}
schedule_url = 'https://stgapi.betradar.com/v1/sports/en/schedules/{date}/schedule.xml'
profile_url = 'https://stgapi.betradar.com/v1/sports/en/competitors/{id}/profile.xml'


def get_date(date):
    return datetime.datetime.strptime(date, '%Y-%m-%d')

async def get_soccer_competitors(league_name, start, end):
    current_date = get_date(start)
    end_date = get_date(end)
    dates = []
    while current_date < end_date:
        dates.append(str(current_date.date()))
        current_date = current_date + datetime.timedelta(days=1)
    begin = time.time()
    jerseys = await get_soccer_teams_async(dates, league_name)
    end = time.time()

    print(f"{end - begin} seconds")
    print(json.dumps(jerseys))
    return json.dumps(jerseys)


async def get_soccer_teams_async(dates, league_name):
    teams_result = await asyncio.gather(*[get_teams(date, league_name) for date in dates])
    teams = {item for sublist in teams_result for item in sublist}
    jerseys = await asyncio.gather(*[get_jersey_for_one_team(team) for team in teams])
    return {jersey[0]: jersey[1] for jersey in jerseys}

async def get_teams(date, league_name):
    response = await requests.get(schedule_url.format(date=date), headers=headers)
    result = xmltodict.parse(response.content)
    competitors = [event['competitors']['competitor'] for event in result['schedule']['sport_event'] if
                   'tournament' in event and event['tournament']['sport']['@name'] == 'Soccer' and
                   'season' in event and event['season']['@name'] == league_name]
    return set([(comp['@id'], comp['@name']) for sublist in competitors for comp in sublist])


async def get_jersey_for_one_team(team_tuple):
    response = await requests.get(url=profile_url.format(id=team_tuple[0]), headers=headers)
    jerseys = xmltodict.parse(response.content)['competitor_profile']['jerseys']['jersey']
    home_jersey = [jersey for jersey in jerseys if jersey['@type'] == 'home']
    return team_tuple[1], transform_jersey(home_jersey[0])


def transform_jersey(jersey_json):
    new_json = {}

    for key, value in jersey_json.items():
        new_key = key
        if '@' in key:
            new_key = key.replace('@', '')
        if value == "true":
            new_json[new_key] = True
        elif value == "false":
            new_json[new_key] = False
        else:
            new_json[new_key] = value
    return new_json
